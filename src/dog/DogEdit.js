import React, {Component} from 'react';
import {Text, TouchableOpacity, Image, View, TextInput, ActivityIndicator} from 'react-native';
import {saveDog, deleteDog, cancelSaveDog} from './service';
import {registerRightAction, issueToText, getLogger} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('DogEdit');
const DOG_EDIT_ROUTE = 'dog/edit';

export class DogEdit extends Component {
    static get routeName() {
        return DOG_EDIT_ROUTE;
    }

    static get route() {
        return {name: DOG_EDIT_ROUTE, title: 'Dog Edit', rightText: 'Save'};
    }

    constructor(props) {
        log('constructor');
        super(props);
        this.imgUrl = null;
        this.store = this.props.store;
        const nav = this.props.navigator;
        this.navigator = nav;
        const currentRoutes = nav.getCurrentRoutes();
        const currentRoute = currentRoutes[currentRoutes.length - 1];
        if (currentRoute.data) {
            this.state = {dog: {...currentRoute.data}, isSaving: false, isDeleting: false};
        } else {
            this.state = {dog: {text: ''}, isSaving: false, isDeleting: false};
        }
        registerRightAction(nav, this.onSave.bind(this));
    }

    render() {
        log('render');
        const state = this.state;
        let message = issueToText(state.issue);

        return (
            <View style={styles.dogEditContent}>
                { state.isSaving &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                <Text>Photo</Text>
                {state.dog.img
                    ? <Image
                        style={{width: 280, height: 280}}
                        source={{uri: state.dog.img}}/>
                    : null}
                <Text>img Url: </Text>
                <TextInput value={state.dog.img} onChangeText={(img) => this.imgUrl = img}></TextInput>
                <Text>Name</Text>
                <TextInput value={state.dog.text} onChangeText={(text) => this.updateDogText(text)}></TextInput>
                {message && <Text>{message}</Text>}

                <TouchableOpacity
                    style={{width: 100, height: 50}}
                    onPress={() => this.onDeleteDog()}>
                    <Text>Delete</Text>
                </TouchableOpacity>
            </View>
        );
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.props.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const state = this.state;
            const dogState = store.getState().dog;
            this.setState({...state, issue: dogState.issue});
        });
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        if (this.state.isLoading) {
            this.store.dispatch(cancelSaveDog());
        }
    }

    updateDogText(text) {
        let newState = {...this.state};
        newState.dog.text = text;
        this.setState(newState);
    }

    updateDogImg(img) {
        let newState = {...this.state};
        newState.dog.img = img;
        this.setState(newState);
    }

    onDeleteDog() {
        log('onDelete');
        this.store.dispatch(deleteDog(this.state.dog)).then(() => {
            log('onDogDeleted');
            if (!this.state.issue) {
                this.navigator.pop();
            }
        });
    }

    onSave() {
        log('onSave');
        if (this.imgUrl)
            this.updateDogImg(this.imgUrl);
        this.store.dispatch(saveDog(this.state.dog)).then(() => {
            log('onDogSaved');
            if (!this.state.issue) {
                this.navigator.pop();
            }
        });
    }
}