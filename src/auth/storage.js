import {AsyncStorage} from 'react-native';
import {getLogger} from '../core/utils';

const USER = 'user';
const TOKEN = 'token';
const SERVER = 'server';

const log = getLogger('auth/storage');

export const readUser = async() => await read(USER);
export const saveUser = async(user) => await save(USER, user);
export const removeUser = async() => {await remove(USER); await remove(TOKEN)};

export const saveToken = async(token) => await save(TOKEN, token);
export const readToken = async() => await read(TOKEN);

export const readServer = async() => await read(SERVER);
export const saveServer = async(server) => await save(SERVER, server);


export const save = async(key, object) => {
    log(`save ${key}`);
    await AsyncStorage.setItem(key, JSON.stringify(object));
};

export const read = async(key) => {
    log(`read ${key}`);
    return JSON.parse(await AsyncStorage.getItem(key));
};

export const remove = async(key) => {
    log(`remove ${key}`);
    return JSON.parse(await AsyncStorage.removeItem(key));
};