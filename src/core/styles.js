/**
 * Created by dan on 24/10/16.
 */
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    content: {
        marginTop: 70,
        flex: 1
    },
    activityIndicator: {
        height: 50
    },
    dogEditContent: {
        width: 300,
        marginTop: 70,
        alignSelf: "center",
    },
    list: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        flexWrap: "wrap"
    }
});

export default styles;