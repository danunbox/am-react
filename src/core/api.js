/**
 * Created by dan on 24/10/16.
 */
export const serverUrl = 'http://192.168.0.40:3000';
export const headers = {'Accept': 'application/json', 'Content-Type': 'application/json'};
export const authHeaders = (token) => ({...headers, 'Authorization': `Bearer ${token}`});
